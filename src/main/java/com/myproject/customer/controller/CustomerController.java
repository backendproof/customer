package com.myproject.customer.controller;

import com.myproject.customer.model.Customer;
import com.myproject.customer.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/customer")
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    @GetMapping(path = "/add")
    public @ResponseBody String addNewCustomer (@RequestParam String documentType,
                                                @RequestParam long documentNumber,
                                                @RequestParam String email) {
        Customer customer = new Customer();
        customer.setDocumentType(Customer.DocumentType.valueOf(documentType));
        customer.setDocumentNumber(documentNumber);
        customer.setEmail(email);

        customerRepository.save(customer);
        return "Saved";
    }

    @GetMapping(path = "/remove")
    public @ResponseBody String removeCustomer(@RequestParam int id) {
        customerRepository.delete(id);
        return "Removed";
    }
}
